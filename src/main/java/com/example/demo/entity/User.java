package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "users")
public class User {
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "account")
	private String account;
	@Column(name = "password")
	private String password;
	@NotEmpty(message = "ユーザー名を入力してください")
	@Size(max = 10, message = "ユーザー名は10文字以下で入力してください。")
	@Column(name = "name")
	private String name;

	@Column(name = "branch_id")
	private int brancheId;

	@Column(name = "department_id")
	private int departmentId;

	@Column(name = "is_stopped")
	private int isStopped;

	@CreationTimestamp
	@Column(name = "created_date", nullable = false, updatable = false)
	private Date createdDate;

	@UpdateTimestamp
	@Column(name = "updated_date", nullable = false)
	private Date updatedDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public int getBrancheId() {
		return brancheId;
	}

	public void setBrancheId(int brancheId) {
		this.brancheId = brancheId;
	}

	public int getIsStopped() {
		return isStopped;
	}

	public void setIsStopped(int isStopped) {
		this.isStopped = isStopped;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@AssertTrue(message = "支社と部署の組み合わせが不正です")
	public boolean isBrancDepartment() {
		if (brancheId == 1 && departmentId == 1) {
			return true;
		}
		if (brancheId == 1 && departmentId == 2) {
			return true;
		}
		if (brancheId != 1 && departmentId == 3) {
			return true;
		}
		if (brancheId != 1 && departmentId == 4) {
			return true;
		}
		return false;
	}

	@AssertTrue(message = "アカウントを入力してください")
	public boolean isAccountNull() {
		if (account.isBlank()) {
			return false;
		}
		return true;
	}

	@AssertTrue(message = "アカウントは6文字以上で入力してください")
	public boolean isAccountMin() {
		if (!account.isBlank()) {
			if (account.length() < 6) {
				return false;
			}
		}
		return true;
	}

	@AssertTrue(message = "アカウントは20文字以下で入力してください")
	public boolean isAccountMax() {
		if (!account.isBlank()) {
			if (account.length() > 20) {
				return false;
			}
		}
		return true;
	}

	@AssertTrue(message = "アカウントは半角英数字で入力してください。")
	public boolean isAccountLetter() {
		if (!account.isBlank()) {
			if (!account.matches("^[A-Za-z0-9]+$")) {
				return false;
			}
		}
		return true;
	}

}
