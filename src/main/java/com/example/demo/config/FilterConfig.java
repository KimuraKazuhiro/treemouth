package com.example.demo.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.filter.LoginFilter;
import com.example.demo.filter.PrivilegeFilter;

@Configuration

public class FilterConfig {
	@Bean
	public FilterRegistrationBean<LoginFilter> hogeFilter() {
		FilterRegistrationBean<LoginFilter> bean = new FilterRegistrationBean<LoginFilter>(new LoginFilter());
		bean.addUrlPatterns("/treeMouth/user", "/treeMouth/user/*", "/treeMouth/startUser/*", "/treeMouth/signUp",
				"/treeMouth",
				"/treeMouth/message", "/treeMouth/message/", "/treeMouth/comment",
				"/treeMouth/comment/*",
				"/treeMouth/deleteComment/*");

		bean.setOrder(1);
		return bean;
	}

	@Bean
	public FilterRegistrationBean<PrivilegeFilter> fugaFilter() {
		FilterRegistrationBean<PrivilegeFilter> bean = new FilterRegistrationBean<PrivilegeFilter>(
				new PrivilegeFilter());
		bean.addUrlPatterns("/treeMouth/user", "/treeMouth/user/*", "/treeMouth/signUp");

		bean.setOrder(2);
		return bean;
	}
}
