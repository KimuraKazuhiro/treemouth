package com.example.demo.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.entity.User;

public class PrivilegeFilter implements Filter {
	@Autowired
	HttpSession session;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		HttpSession session = req.getSession();

		User loginUser = (User) session.getAttribute("loginUser");
		if (loginUser != null) {
			if (loginUser.getDepartmentId() == 1 && loginUser.getBrancheId() == 1) {
				chain.doFilter(request, response);
			} else {
				List<String> errorList = new ArrayList<String>();
				errorList.add("権限がありません。");
				session.setAttribute("errorList", errorList);
				res.sendRedirect("/treeMouth/");
			}

		} else {
			List<String> errorList = new ArrayList<String>();
			errorList.add("ログインしてください。");
			session.setAttribute("errorList", errorList);
			res.sendRedirect("/treeMouth/login");
		}
	}

	@Override
	public void destroy() {
	}
}