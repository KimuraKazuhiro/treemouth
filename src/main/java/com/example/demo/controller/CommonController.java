package com.example.demo.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Message;
import com.example.demo.entity.User;
import com.example.demo.entity.UserBranchDepartment;
import com.example.demo.service.CommentService;
import com.example.demo.service.MessageService;
import com.example.demo.service.UserService;

@Controller

public class CommonController extends HttpServlet {

	@Autowired
	HttpSession session;
	@Autowired
	MessageService messageService;
	@Autowired
	CommentService commentService;
	@Autowired
	UserService userService;

	//全件取得
	@GetMapping("/treeMouth")
	public ModelAndView top() {
		ModelAndView mav = new ModelAndView();
		List<UserBranchDepartment> userData = userService.select();

		List<Message> messageData = messageService.findAllMessage();
		List<Comment> commentData = commentService.findAllComment();
		User user = (User) session.getAttribute("loginUser");
		mav.setViewName("/top");
		mav.addObject("messages", messageData);
		mav.addObject("comments", commentData);
		mav.addObject("users", userData);
		mav.addObject("loginUser", user);
		return mav;
	}

	//日付絞り込み
	@PostMapping("date")
	public ModelAndView dateMessage(@RequestParam(name = "startDate", required = false) String startDate,
			@RequestParam(name = "endDate", required = false) String endDate,
			@RequestParam(name = "searchCategory", required = false) String searchCategory) {

		ModelAndView mav = new ModelAndView();
		String start = null;
		String end = null;

		if (startDate != null && !startDate.equals("")) {
			start = startDate + " 00:00:00";
		} else {
			start = "2020-01-01 00:00:00";
		}
		if (endDate != null && !endDate.equals("")) {
			end = endDate + " 23:59:59";
		} else {
			Calendar cal1 = Calendar.getInstance();
			Date date = (Date) cal1.getTime();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			end = dateFormat.format(date);
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		try {
			Date StartDateFormat = new Date(sdf.parse(start).getTime());
			Date endDateFormat = new Date(sdf.parse(end).getTime());

			if (!searchCategory.isBlank()) {
				List<Message> messageData = messageService
						.findByCategoryContainingAndCreatedDateBetweenOrderByCreatedDateDesc(searchCategory,
								StartDateFormat, endDateFormat);
				mav.addObject("messages", messageData);
			} else {
				List<Message> messageData = messageService
						.findByCreatedDateBetweenOrderByCreatedDateDesc(StartDateFormat, endDateFormat);
				mav.addObject("messages", messageData);
			}
			mav.addObject("startDate", startDate);
			mav.addObject("endDate", endDate);

		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		User user = (User) session.getAttribute("loginUser");
		List<Comment> commentData = commentService.findAllComment();
		mav.setViewName("/top");
		mav.addObject("comments", commentData);
		mav.addObject("loginUser", user);
		mav.addObject("searchCategory", searchCategory);
		return mav;
	}

}
