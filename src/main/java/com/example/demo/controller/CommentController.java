package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.User;
import com.example.demo.service.CommentService;

@Controller
public class CommentController {
	@Autowired
	CommentService commentService;
	@Autowired
	HttpSession session;

	//投稿処理
	@PostMapping("/comment")
	public ModelAndView addComment(@RequestParam(name = "messageId") Integer messageId, @RequestParam(name = "text") String text) {

		Comment comment = new Comment();
		List<String> errorList = new ArrayList<String>();

		if (text.isBlank()) {
			errorList.add("コメントは入力してください。");
		}
		if (text.length() >= 500) {
			errorList.add("コメントは500文字以内で入力してください。");
		}

		if (errorList.size() == 0) {
			comment.setMessageId(messageId);
			comment.setText(text);
			User user = (User)session.getAttribute("loginUser");
			comment.setUserId(user.getId());
			commentService.saveComment(comment);
		}

		session.setAttribute("errorList", errorList);
		return new ModelAndView("redirect:/treeMouth");
	}

	//削除処理
	@DeleteMapping("/deleteComment/{commentId}")
	public ModelAndView deleteComment(@PathVariable Integer commentId) {
		commentService.deleteComment(commentId);
		return new ModelAndView("redirect:/treeMouth");
	}

}
