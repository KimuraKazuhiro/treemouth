package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Message;
import com.example.demo.entity.User;
import com.example.demo.service.MessageService;

@Controller
public class MessageController {
	@Autowired
	MessageService messageService;
	@Autowired
	HttpSession session;

	//投稿画面表示
	@GetMapping("/treeMouth/message")
	public ModelAndView newMessage() {
		ModelAndView mav = new ModelAndView();
		Message message = new Message();
		mav.setViewName("/message");
		mav.addObject("messageModel", message);
		return mav;
	}

	//投稿処理
	@PostMapping("/addMessage")
	public ModelAndView addMessage(@Valid @ModelAttribute("messageModel") Message message, BindingResult result,
			Model model) {
		List<String> errorList = new ArrayList<String>();
		if (message.getText().isBlank()) {
			errorList.add("本文を入力してください");
		}
		if (message.getTitle().isBlank()) {
			errorList.add("件名を入力してください");
		}
		if (message.getCategory().isBlank()) {
			errorList.add("カテゴリーを入力してください");
		}
		if (result.hasErrors()) {
			for (ObjectError error : result.getAllErrors()) {
				errorList.add(error.getDefaultMessage());
			}
		}

		if (errorList.size() != 0) {
			model.addAttribute("validationError", errorList);
			ModelAndView mav = new ModelAndView();
			mav.setViewName("/message");
			mav.addObject("messageModel", message);
			return mav;
		}

		User user = (User) session.getAttribute("loginUser");
		message.setUserId(user.getId());
		messageService.saveMessage(message);
		return new ModelAndView("redirect:/treeMouth");
	}

	//削除処理
	@DeleteMapping("deleteMessage/{messageId}")
	public ModelAndView deleteMessage(@PathVariable Integer messageId) {
		messageService.deleteMessage(messageId);
		return new ModelAndView("redirect:/treeMouth");
	}

}
