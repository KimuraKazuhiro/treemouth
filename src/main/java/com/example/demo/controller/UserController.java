package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Branche;
import com.example.demo.entity.Department;
import com.example.demo.entity.User;
import com.example.demo.entity.UserBranchDepartment;
import com.example.demo.service.UserService;

@Controller
public class UserController {

	@Autowired
	UserService userService;

	@Autowired
	HttpSession session;

	// 新規登録画面
	@GetMapping("/treeMouth/signUp")
	public ModelAndView registerUser() {
		List<Branche> brancheData = userService.findAllBranche();
		List<Department> departmentData = userService.findAllDepartment();

		ModelAndView mav = new ModelAndView();
		User user = new User();
		String checkPassword = null;

		mav.setViewName("/signUp");
		mav.addObject("formModel", user);
		mav.addObject("checkPassword", checkPassword);
		mav.addObject("brancheData", brancheData);
		mav.addObject("departmentData", departmentData);
		return mav;
	}

	// 新規登録処理
	@PostMapping("/signUp")
	public ModelAndView registerUserPostw(@Valid @ModelAttribute("formModel") User user, BindingResult result,
			Model model, @ModelAttribute("checkPassword") String checkPassword) {
		List<String> errorList = new ArrayList<String>();
		User accountCheckUser = userService.select(user.getAccount());
		if (accountCheckUser != null) {
			errorList.add("アカウントが重複しています");
		}

		if (!user.getPassword().equals(checkPassword)) {
			errorList.add("入力したパスワードと確認用パスワードが一致しません");
		}
		if (user.getPassword().length() >= 21) {
			errorList.add("パスワードは20文字以下で入力してください");
		}
		if (user.getPassword().length() <= 5 && user.getPassword().length() >= 1) {
			errorList.add("パスワードは6文字以上で入力してください");
		}
		if (user.getPassword().length() == 0) {
			errorList.add("パスワードを入力してください");
		}
		if (result.hasErrors()) {
			for (ObjectError error : result.getAllErrors()) {
				errorList.add(error.getDefaultMessage());
			}
		}

		if (errorList.size() != 0) {
			List<Branche> brancheData = userService.findAllBranche();
			List<Department> departmentData = userService.findAllDepartment();
			model.addAttribute("validationError", errorList);
			ModelAndView mav = new ModelAndView();
			mav.setViewName("/signUp");
			mav.addObject("formModel", user);
			mav.addObject("brancheData", brancheData);
			mav.addObject("departmentData", departmentData);
			return mav;
		}
		// 投稿をテーブルに格納
		userService.saveUser(user);
		// rootへリダイレクト
		return new ModelAndView("redirect:/treeMouth/user");
	}

	//ユーザー編集画面
	@GetMapping("/treeMouth/user/edit/{id}")
	public ModelAndView editUser(@PathVariable String id) {

		if (!id.matches("[0-9]*")) {
			List<String> errorList = new ArrayList<String>();
			errorList.add("不正なパラメータが入力されました");
			session.setAttribute("errorList", errorList);
			return new ModelAndView("redirect:/treeMouth/user");
		}
		List<Branche> brancheData = userService.findAllBranche();
		List<Department> departmentData = userService.findAllDepartment();
		ModelAndView mav = new ModelAndView();
		User user = userService.select(Integer.parseInt(id));

		if (user == null) {
			List<String> errorList = new ArrayList<String>();
			errorList.add("不正なパラメータが入力されました");
			session.setAttribute("errorList", errorList);
			return new ModelAndView("redirect:/treeMouth/user");
		}
		mav.setViewName("/userEdit");
		mav.addObject("formModel", user);
		mav.addObject("brancheData", brancheData);
		mav.addObject("departmentData", departmentData);
		return mav;
	}

	@GetMapping("/treeMouth/user/edit/")
	public ModelAndView editUser() {
		List<String> errorList = new ArrayList<String>();
		errorList.add("不正なパラメータが入力されました");
		session.setAttribute("errorList", errorList);
		return new ModelAndView("redirect:/treeMouth/user");
	}

	// ユーザー編集処理
	@PostMapping("/userEdit")
	public ModelAndView registerUserPost(@Valid @ModelAttribute("formModel") User user, BindingResult result,
			Model model, @ModelAttribute("checkPassword") String checkPassword) {

		List<String> errorList = new ArrayList<String>();
		User accountCheckUser = userService.select(user.getAccount());
		if (accountCheckUser != null) {
			if (accountCheckUser.getId() != user.getId()) {
				errorList.add("アカウントが重複しています。");
			}
		}

		if (!user.getPassword().equals(checkPassword)) {
			errorList.add("入力したパスワードと確認用パスワードが一致しません");
		}
		if (user.getPassword().length() >= 21) {
			errorList.add("パスワードは20文字以下で入力してください");
		}
		if (user.getPassword().length() <= 5 && user.getPassword().length() >= 1) {
			errorList.add("パスワードは6文字以上で入力してください");
		}

		if (result.hasErrors()) {
			for (ObjectError error : result.getAllErrors()) {
				errorList.add(error.getDefaultMessage());
			}
		}

		if (errorList.size() != 0) {
			List<Branche> brancheData = userService.findAllBranche();
			List<Department> departmentData = userService.findAllDepartment();
			model.addAttribute("validationError", errorList);
			ModelAndView mav = new ModelAndView();
			mav.setViewName("/userEdit");
			mav.addObject("formModel", user);
			mav.addObject("brancheData", brancheData);
			mav.addObject("departmentData", departmentData);
			return mav;
		}
		// 投稿をテーブルに格納
		userService.saveUser(user);
		// rootへリダイレクト
		return new ModelAndView("redirect:/treeMouth/user");
	}

	// ログイン画面
	@GetMapping("/treeMouth/login")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView();
		User user = new User();
		mav.setViewName("/login");
		mav.addObject("formModel", user);
		return mav;
	}

	// ログイン処理
	@PostMapping("/login")
	public ModelAndView loginUser(@ModelAttribute("formModel") User user) {

		String account = user.getAccount();
		String password = user.getPassword();

		User loginUser = userService.select(account, password);
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();

		if (account.isBlank()) {
			errorMessages.add("アカウントが入力されていません");
		}
		if (password.isBlank()) {
			errorMessages.add("パスワードが入力されていません");

		}
		if (loginUser == null && !password.isBlank() && !account.isBlank()) {
			errorMessages.add("アカウントまたはパスワードが誤っています");
		}

		if (errorMessages.size() != 0) {
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("loginUser", null);
			mav.setViewName("/login");
			mav.addObject("formModel", user);
			return mav;
		}

		session.setAttribute("loginUser", loginUser);
		return new ModelAndView("redirect:/treeMouth");
	}

	// ログアウト
	@GetMapping("/treeMouth/logout")
	public ModelAndView logout() {
		session.invalidate();
		return new ModelAndView("redirect:/treeMouth/login");
	}

	// ユーザー一覧表示画面
	@GetMapping("/treeMouth/user")
	public ModelAndView user() {
		ModelAndView mav = new ModelAndView();
		List<UserBranchDepartment> users = userService.select();
		mav.setViewName("/user");
		mav.addObject("users", users);
		return mav;
	}

	// ユーザー再開
	@GetMapping("/treeMouth/startUser/{id}")
	public ModelAndView startUser(@PathVariable int id) {
		User user = userService.select(id);
		user.setIsStopped(0);
		userService.saveUser(user);
		ModelAndView mav = new ModelAndView();
		List<UserBranchDepartment> users = userService.select();
		mav.setViewName("/user");
		mav.addObject("users", users);
		return mav;
	}

	// ユーザー停止
	@GetMapping("/treeMouth/stopUser/{id}")
	public ModelAndView stopUser(@PathVariable int id) {
		User loginUser = (User) session.getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<String>();
		if (loginUser.getId() == id) {
			errorMessages.add("自分のアカウントは変更できません。");
		} else {
			User user = userService.select(id);
			user.setIsStopped(1);
			userService.saveUser(user);
		}
		ModelAndView mav = new ModelAndView();
		List<UserBranchDepartment> users = userService.select();
		mav.addObject("errorMessages", errorMessages);
		mav.setViewName("/user");
		mav.addObject("users", users);
		return mav;
	}

}
