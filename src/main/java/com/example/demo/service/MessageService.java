package com.example.demo.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Message;
import com.example.demo.repository.MessageRepository;

@Service
public class MessageService {
	@Autowired
	MessageRepository messageRepository;

	//投稿
	public void saveMessage(Message message) {
		messageRepository.save(message);
	}

	//全件表示
	public List<Message> findAllMessage() {
		return messageRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
	}

	// 日付絞り込み
	public  List<Message> findByCreatedDateBetweenOrderByCreatedDateDesc(Date startDateFormat, Date endDateFormat) {
		return messageRepository.findByCreatedDateBetweenOrderByUpdatedDateDesc(startDateFormat, endDateFormat);
	}

	//カテゴリー絞り込み
	public List<Message> findByCategoryContaining(String searchCategory){
		return messageRepository.findByCategoryContaining(searchCategory);
	}

	//投稿削除
	public void deleteMessage(Integer messageId) {
		messageRepository.deleteById(messageId);
	}

	public List<Message> findByCategoryContainingAndCreatedDateBetweenOrderByCreatedDateDesc(String searchCategory,
			Date startDateFormat, Date endDateFormat) {
		return messageRepository.findByCategoryContainingAndCreatedDateBetweenOrderByCreatedDateDesc(searchCategory,startDateFormat, endDateFormat);
	}

}
