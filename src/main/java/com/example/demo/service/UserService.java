package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Branche;
import com.example.demo.entity.Department;
import com.example.demo.entity.User;
import com.example.demo.entity.UserBranchDepartment;
import com.example.demo.repository.BrancheRepository;
import com.example.demo.repository.DepartmentRepository;
import com.example.demo.repository.UserBranchDepartmentRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.utils.CipherUtil;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	@Autowired
	BrancheRepository brancheRepository;
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
	UserBranchDepartmentRepository userBranchDepartmentRepository;

	public void saveUser(User user) {
		if (user.getPassword().isBlank()) {
			User oldUser = select(user.getId());
			user.setPassword(oldUser.getPassword());
		} else {
			// パスワード暗号化
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);
		}

		userRepository.save(user);
	}

	public User select(String account, String password) {
		// パスワード暗号化
		String encPassword = CipherUtil.encrypt(password);

		User user = userRepository.findByAccount(account);
		if (user != null) {
			if (!user.getPassword().equals(encPassword)) {
				user = null;
			}
		}
		if (user != null) {
			if (user.getIsStopped() == 1) {
				user = null;
			}
		}
		return user;
	}

	public User select(int id) {
		User user = userRepository.findById(id).orElse(null);
		return user;
	}

	public User select(String account) {
		User user = userRepository.findByAccount(account);

		return user;
	}

	public List<UserBranchDepartment> select() {
		Sort sort = Sort.by(Sort.Direction.ASC, "id");
		List<UserBranchDepartment> users = userBranchDepartmentRepository.findAll(sort);
		return users;
	}

	public List<Branche> findAllBranche() {
		return brancheRepository.findAll();
	}

	public List<Department> findAllDepartment() {
		return departmentRepository.findAll();
	}

	public List<User> findAllUser() {
		return userRepository.findAll();
	}
}
