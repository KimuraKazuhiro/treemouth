/**
 *
 */

$(function() {
	var topBtn = $('.toTop');
	$(window).scroll(function() {
		if ($(this).scrollTop() > 100) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
	});
	topBtn.click(function() {
		$('body,html').animate({
			scrollTop : 0
		}, 500);
		return false;
	});

});

function inputLength(account, name) {
	console.log(account);
	document.getElementsByClassName("accountBox")[0].innerHTML = account.length
			- 2 + "/" + "20";
	if (account.length > 20 || account.length < 6) {
		document.getElementsByClassName("accountBox")[0].style.color = "red";
	} else {
		document.getElementsByClassName("accountBox")[0].style.color = "black";
	}

	document.getElementsByClassName("nameBox")[0].innerHTML = name.length - 2
			+ "/" + "10";
	if (name.length > 11 || name.length < 1) {
		document.getElementsByClassName("nameBox")[0].style.color = "red";
	} else {
		document.getElementsByClassName("nameBox")[0].style.color = "black";
	}
}

function showLength(str, className, min, max) {
	document.getElementsByClassName(className)[0].innerHTML = str.length + "/"
			+ max;
	if (str.length > max || str.length < min) {
		document.getElementsByClassName(className)[0].style.color = "red";
	} else {
		document.getElementsByClassName(className)[0].style.color = "black";
	}
}
